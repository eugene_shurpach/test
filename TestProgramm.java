import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TestProgramm {

	public static void main(String[] args) {
		
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			System.out.print("Enter array: ");
			String arrayStr = br.readLine();
			System.out.print("Enter step: ");
			String stepStr = br.readLine();
			try{
	            String[] vars = arrayStr.split(" ");
	            int[] array = new int[vars.length];
	            for (int i = 0, size = vars.length; i < size; i++) {
	            	array[i] = Integer.parseInt(vars[i]);
	            }
	            int step = Integer.parseInt(stepStr);
	            int[] result = returnArray(array, step);
				System.out.print("Output:")
				for (int i = 0, size = result.length; i < size; i++) {
					System.out.print(result[i]);
				}
	        }catch(NumberFormatException nfe){
	            System.err.println("Invalid Format!");
	        }
    	} catch (IOException e) {

    	}
	}

	private static int[] returnArray(int[] array, int n) {
		int[] result = new int[array.length];
		for (int i = 0, size = array.length; i < size; i++) {
			result[countNextIndex(i, n, size)] = array[i];
		}
		return result;
	}

	private static int countNextIndex(int prev, int step, int length) {
		int temp = prev + step;
		if (temp >= length) {
			temp %= length;
		}
		return temp;
	}

}
